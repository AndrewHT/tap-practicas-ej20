
package practicas;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.*;


public class Practica1 extends JFrame implements ActionListener {

    
    JLabel etiq1;
    JTextField nombre;
    JButton btn; 
    
   Practica1(){
           
    this.setTitle("Práctica 1");
    this.setSize(300,150);
    this.setLayout(new FlowLayout());
    this.setLocationRelativeTo(null);
    
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

  etiq1 = new JLabel("Escriba un nombre para saludar");
   nombre = new JTextField(20);
    btn = new JButton("¡Saludar!");
    
    btn.addActionListener(this);
    
    this.add(etiq1);
    this.add(nombre);
    this.add(btn);
   }
    
 
 public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Practica1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Practica1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Practica1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Practica1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Practica1().setVisible(true);
            }
        });
 }
 
 public void actionPerformed(ActionEvent e){
     
     
     JOptionPane.showMessageDialog(this, "¡Hola " + this.nombre.getText()+"!");
     
     
 }
 
 
 
 
 
 
}
    
    


