
package practicas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;


public class Practica_de_muestra {

 
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(500,300);
        frame.setLayout(new FlowLayout());
        frame.setLocationRelativeTo(null);
        
        JButton botón1 = new JButton("Cerrar");
        botón1.setSize(250, 250);
        
        botón1.addActionListener((ActionEvent e) -> {
               System.exit(0);
        });
        frame.add(botón1);
        
        frame.setVisible(true);
    }
    
}
